# %%
import os
import re
import pandas as pd
import datetime
from lxml import etree


def fileDefinder(KeyWords="", path="htmls", ftype="html"):
    fList = []
    for p in os.listdir(path):
        if KeyWords in p and ftype in p:
            # print(p)
            fList.append(path + "/" + p)
    return fList


def XpNotNall(xpathRes):
    return None if xpathRes is None or xpathRes == [] else xpathRes


def TmpLoop(tmpList: list, types: str):
    for t in tmpList:
        t["types"] = types
    return tmpList


def MainLoop(filepaths):
    # print(filepaths)
    with open(filepaths, encoding="utf") as f:
        file = f.read()

    html = etree.HTML(file)
    sectionList = html.xpath(
        "//div[@id='js_content']//section[@data-tools='135编辑器']//p"
    )

    date = ""
    dis = ""

    MainList = []
    tmpList = []
    d = {}
    for s in sectionList:
        text = "".join(s.xpath(".//text()"))
        if text:
            t_date = re.findall(r"(\d+)年(\d+)月(\d+)日", text)
            date = datetime.date(
                int(t_date[0][0]), int(t_date[0][1]), int(t_date[0][2])
            ) if t_date != [] else date
            numbers1 = XpNotNall(re.findall(r"病例(\d+)", text))
            numbers2 = XpNotNall(re.findall(r"无症状感染者(\d+)", text))
            numbers = numbers1 if numbers1 else numbers2

            if numbers and "居住" in text:
                dailyNumber = (
                    int(numbers[1]) + 1 - int(numbers[0]) if len(numbers) > 1 else 1
                )
                dis = XpNotNall(re.findall(r"(?<=居住于)(.+区)", text))

                d = {
                    "dis": dis[0].replace("，", "") if dis else None,
                    "value": dailyNumber,'date':date
                }
                # if not dis:
                #     print(text)
                # print(dailyNumber, dis, end='')
                tmpList.append(d)
            if "闭环" in text and "复核" in text:
                # print(tmpList)
                types = "闭环" + ("确诊" if "无症状" not in text else "无症状")
                MainList.extend(TmpLoop(tmpList=tmpList, types=types))
                tmpList = []
            elif "风险" in text and "复核" in text:
                # print(tmpList)
                types = "风险筛查" + ("确诊" if "无症状" not in text else "无症状")
                MainList.extend(TmpLoop(tmpList=tmpList, types=types))
                tmpList = []
            elif "此前" in text and "无症状" in text:
                # print(tmpList)
                types = "转归"
                MainList.extend(TmpLoop(tmpList=tmpList, types=types))
                tmpList = []
        if "5月10日" in filepaths:
            print(text,d)

    return MainList


l = []
filepaths = fileDefinder(KeyWords="例", path="SourceHTMLs/DailySummary", ftype="htm")
# filepaths2 = fileDefinder(KeyWords="例", path="SourceHTMLs/DailySummary", ftype="html")
filepaths.extend(filepaths)
set(filepaths)
#%%
for f in set(filepaths):
    l.extend(MainLoop(f))

l
#%%
# return dfAdd, dfDaily
df = pd.DataFrame(l)
df[df['date'] == datetime.date(2022,4,22)]
df['value'] = df['value'].astype(float)
dfGB = df.groupby(by=['date','dis','types']).sum()
dfGB.reset_index(inplace=True)
df = dfGB.pivot(index=['date',"dis"], columns="types", values="value").fillna(0)
df.reset_index(inplace=True)

dfNextDay = df[df['date'] == df['date'].max()][['date','dis']]
dfNextDay['date'] = dfNextDay['date'] + pd.Timedelta(days=1)

df = pd.concat([df,dfNextDay]).fillna(0)
# df[df['date'] == datetime.date(2022,4,22)]
df.to_csv("DataSource/dailyDetial.csv",index=False)

#%%
df.iloc[-30:]
#%%
