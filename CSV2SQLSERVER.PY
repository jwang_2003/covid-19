# encoding=utf-8
#%%
import pymssql
import pandas as pd

#%%
def SqlInput(
    DataList: list,
    inputTableName: str,
    sqlSetting: dict,
    keyMap: dict,
    batchSize=999,
    istrans=True,
):
    conn = pymssql.connect(
        sqlSetting["server"],
        sqlSetting["user"],
        sqlSetting["password"],
        sqlSetting["database"],
        charset="utf8",
    )
    cursor = conn.cursor()

    insertQuery = """Insert Into {} {} values 
    """.upper()

    t = []
    for i in DataList[0].keys():
        t.append(keyMap.get(i, i))

    tableColumn = str(tuple(t)).replace("'", "")

    cursor.execute("TRUNCATE table {}".format(inputTableName))
    conn.commit()
    execQuery, infoQuery = "", ""

    totalLenofTable = len(DataList)
    for i, d in enumerate(DataList):
        if i % batchSize == 0 and i != 0:
            execQuery += (
                insertQuery.format(inputTableName, tableColumn) + infoQuery[:-1] + ";"
            )
            print(
                "{}/{}>> {} >> {}".format(
                    i, totalLenofTable, sqlSetting["database"], inputTableName
                )
            )
            if i != 0 and istrans:
                cursor.execute(execQuery)
                conn.commit()
            else:
                if i > batchSize:
                    print(execQuery)
                    break
            execQuery = ""
            infoQuery = ""
        tmpStr = ""
        for k, v in d.items():
            if "date".upper() in k.upper() or "time".upper() in k.upper() or "create".upper() in k.upper():
                tmpStr += "'{}',".format(v) if v != 'null' else 'null,'
            elif "确诊" in k or "无症状" in k or "转归" in k or inputTableName == 'Omicron_Shanghai.ODS.DailyForecast':
                tmpStr += "{},".format(v) if v != 'null' else 'null,'
            else:
                tmpStr += "N'{}',".format(v) if v != 'null' else 'null,'
        infoQuery += "({}),".format(tmpStr[:-1])
    execQuery += (
            insertQuery.format(inputTableName, tableColumn) + infoQuery[:-1] + ";"
        )
    cursor.execute(execQuery)
    conn.commit()
    print(
    "{}/{}>> {} >> {}".format(
        i+1, totalLenofTable, sqlSetting["database"], inputTableName
        )
    )
    conn.close()


#%%
if __name__ == '__main__':

    sqlSetting = {
        "server": "81.68.245.115",
        "user": "sa",
        "password": "SenseOfBA@021",
        "database": "Omicron_Shanghai",
    }
    keyMap = {
        "road": "Road",
        "dis": "District",
        "Dis": "District",
        "Date": "Dates",
        "date": "Dates",
        "status": "Status",
        "add": "Adderss",
        "street": "Street",
        "business_area":"BusinessArea",
        "type":"Types",
        "确诊":"ConfirmCase",
        "无症状":"Asymptomatic",
        "闭环无症状":"AsymptomaticInCycle",
        "闭环确诊":"ConfirmCaseInCycle",
        "风险筛查无症状":"AsymptomaticRisk",
        "风险筛查确诊":"ConfirmCaseRisk",
        "转归":'Recall'
    }
    #%%
    dfCsv = pd.read_csv("DataSource/test.csv").sort_values(["date", "road"])
    dfDic1 = dfCsv[["road", "dis", "date", "status"]].fillna("").to_dict("record")
    SqlInput(
        dfDic1,
        inputTableName="Omicron_Shanghai.ODS.DailyAdderss",
        sqlSetting=sqlSetting,
        keyMap=keyMap,
        # istrans=False,
    )
    dfDic2 = (
        dfCsv[["road", "loc", "add", "dis", "adcode", "street", "business_area"]]
        .fillna("").drop_duplicates(subset=["road", "loc", "add"])
        .to_dict("record")
    )
    SqlInput(
        dfDic2,
        inputTableName="Omicron_Shanghai.ODS.AdderssList",
        sqlSetting=sqlSetting,
        keyMap=keyMap,
        # istrans=False,
    )
    #%%
    dfCsv = pd.read_csv("DataSource/RequirmentData.csv").sort_values(["createdAt"]).fillna("null")
    dfDic1 = dfCsv.to_dict("record")
    dfDic1
    SqlInput(
        dfDic1,
        inputTableName="Omicron_Shanghai.ODS.DailyRequirment",
        sqlSetting=sqlSetting,
        keyMap=keyMap,
        # istrans=False,
    )
    #%%
    dfCsv = pd.read_csv("DataSource/Daily.csv").sort_values(["Date",'Dis']).fillna("null")
    dfDic1 = dfCsv.to_dict("record")
    SqlInput(
        dfDic1,
        inputTableName="Omicron_Shanghai.ODS.DailySummary",
        sqlSetting=sqlSetting,
        keyMap=keyMap,
        # istrans=False,
        # batchSize=2
    )
    #%%
    dfCsv = pd.read_csv("DataSource/dailyDetial.csv").sort_values(["date",'dis']).fillna("null")
    dfDic1 = dfCsv.to_dict("record")
    SqlInput(
        dfDic1,
        inputTableName="Omicron_Shanghai.ODS.DailyDetial",
        sqlSetting=sqlSetting,
        keyMap=keyMap,
        # istrans=False,
        # batchSize=2
    )
    #%%
    dfCsv = pd.read_csv("DataSource/prod.csv")
    dfCsv = dfCsv[['rowID', 'Prodict', 'runningSum',
        'total', 'Date', 'prod2', 'Value_b', 'Value_c', 'Value_d',
        'error_total', 'error_daliy', 'R2_total', 'R2_daliy', 'pub_date']]
    dfCsv.fillna('null',inplace=True)
    dfDic1 = dfCsv.to_dict("record")
    SqlInput(
        dfDic1,
        inputTableName="Omicron_Shanghai.ODS.DailyForecast",
        sqlSetting=sqlSetting,
        keyMap=keyMap,
        # istrans=False,
        # batchSize=2
    )
#%%

