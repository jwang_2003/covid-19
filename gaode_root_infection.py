# %%
from time import sleep
from tqdm import tqdm
import requests
import json
import pandas as pd
import os

# %%
path = "SourceHTMLs/Data"
dateLimit = "2022-05-08"

# df = pd.read_csv('DataSource/AddList.csv')
df = pd.DataFrame()
for i in os.listdir(path=path):
    if i.split(" ")[0] >= dateLimit:
        df = pd.concat([df, pd.read_excel(path + "/" + i)])
        print(i, df.shape[0])

df.replace({"、": "", "，": "", "。": ""}, regex=True, inplace=True)
df.reset_index(inplace=True)
df.drop(columns="index", inplace=True)
# targetLocationName
if "dis" not in df.columns:
    df["dis"] = " "
df["dis+road"] = df["dis"].fillna(" ") + " " + df["road"]
df["status"].fillna("无症状", inplace=True)
df['date'] = pd.to_datetime(df['date']).dt.date
df.sort_values('date', inplace=True)
_df = df.drop_duplicates(keep="last", subset=["dis+road"])
targetLocationName = _df.to_dict("record")
# %%

print("*" * 50)
l = []

for detials in tqdm(targetLocationName):
    tmp = detials.copy()
    roadName = detials["dis+road"]
    # print(detials)
    roadName = roadName.replace("居住于", "")
    quest_url = "https://restapi.amap.com/v3/geocode/geo?"
    params = {
        "address": roadName,
        "city": "上海",
        "output": "json",
        "key": "28376677293db5afe08b32bf1146314d",
    }
    res = requests.get(url=quest_url, params=params)
    text = json.loads(res.text)
    # print(text)
    if (
        text.get("geocodes") is not None
        and res.status_code == 200
        and text["info"] == "OK"
    ):
        if text["geocodes"] != []:
            t = text["geocodes"][0]
            tmp["loc"] = t["location"]
            tmp["add"] = t["formatted_address"]
            tmp["dis"] = t["district"]
            tmp["adcode"] = t["adcode"]
            tmp["street"] = t["street"]
            tmp["business_area"] = t.get("business_area")
            l.append(tmp)
    else:
        print(
            text.get("geocodes"),
            res.status_code,
            text.get("info"),
            "\n",
            text,
            "\n",
            params,
            "\n",
            tmp
        )
        l.append(detials)
    sleep(1 / 190)

dfadds = pd.DataFrame(l)
# %%
dfadds = dfadds[["road", "loc", "add", "dis", "adcode", "street", "business_area"]]
dfadds.to_csv('DataSource/AddMapInfor.csv',index=False)
_df = df[["road", "dis", "date", "status"]]
dfRes = pd.merge(_df, dfadds, left_on=["road", "dis"], right_on=["road", "dis"])
#%%

preTable = pd.read_csv("DataSource/test.csv")
preTable.drop_duplicates(inplace=True)
preTable

#%%
dfRes = pd.concat([preTable, dfRes])
dfRes["status"].fillna("无症状", inplace=True)
dfRes.sort_values(by=["road", "date"], ascending=True)
_df = dfRes[
    ["road", "loc", "add", "dis", "adcode", "street", "business_area"]
].drop_duplicates(subset=["road", "dis"], keep="last")
dfRes = dfRes[["road", "dis", "date", "status"]]
dfRes = pd.merge(dfRes, _df, left_on=["road", "dis"], right_on=["road", "dis"])
dfRes["date"] = pd.to_datetime(dfRes["date"]).dt.date
dfRes.drop_duplicates(inplace=True, subset=["road", "date"])
dfRes.info()
dfRes.to_csv("DataSource/test.csv", index=False)
print("done")
#%%
