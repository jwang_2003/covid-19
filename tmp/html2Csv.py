# %%
import os
import re
import pandas as pd
import datetime
from lxml import etree


def fileDefinder(KeyWords="", path="htmls", ftype="html"):
    fList = []
    for p in os.listdir(path):
        if KeyWords in p and p.endswith(ftype):
            fList.append(p)
    return fList


def loadText(filepath=""):
    with open(filepath, encoding='utf') as f:
        file = f.read()
    file

    html = etree.HTML(file)

    textL = html.xpath("//div[@class='Article_content']//text()")
    text = "".join(textL)
    return text


def context(con):
    valueL = re.findall(r"\d*", con[0])
    tmpl = []
    for v in valueL:
        if v == "" or v is None:
            pass
        else:
            tmpl.append(int(v))
    t1 = "确诊" if "病例" in con[0] else "无症状"
    num = int(tmpl[1]) - int(tmpl[0]) + 1 if len(tmpl) > 1 else 1
    return {"dis": con[1], "number": num, "Dtype": t1}


def DataReMainCountext(text: str, Patten_dic: dict):

    t = []
    daily1 =  re.findall(pattern=Patten_dic["Patten_daily"], string=text)[0]
    daily2 =  re.findall(pattern=Patten_dic["Patten_daily2"], string=text)[0]
    t.extend(daily1)
    t.extend(daily2)
    dailyPositive = dict(
        zip(
            ["确诊病例", "无症状感染者", "隔离确诊病例", "隔离无症状"],t
            # re.findall(pattern=Patten_dic["Patten_daily"], string=text)[0],
        )
    )
    reinter = (
        re.findall(pattern=Patten_dic["Patten_Reload"], string=text)[0]
        if re.findall(pattern=Patten_dic["Patten_Reload"], string=text) != []
        else 0
    )
    leave = (
        re.findall(pattern=Patten_dic["Patten_Out"], string=text)[0]
        if re.findall(pattern=Patten_dic["Patten_Out"], string=text) != []
        else 0
    )
    dailyPositive["callBack"] = int(reinter)
    dailyPositive["leave"] = int(leave)
    dailyPositive["dis"] = "上海市"
    return dailyPositive


def dic2Table(tmp):
    dfinter = pd.DataFrame(tmp)
    display(dfinter[dfinter['dis']=='闵行区'])
    dfTotal = dfinter.groupby(by=["date", "dis", "Dtype"]).sum().reset_index()
    dfTotal = (
        dfTotal.pivot(index=["date", "dis"], columns="Dtype", values="number")
        .reset_index()
        .fillna(0)
    )
    dfTotal.columns = ["date", "dis", "无症状感染者", "确诊病例"]
    # display(dfTotal)
    if dfinter[dfinter["conType"] == "闭环管控"].shape[0] > 1:
        dfCon = (
            dfinter[dfinter["conType"] == "闭环管控"]
            .groupby(by=["date", "dis", "Dtype"])
            .sum()
            .fillna(0)
            .reset_index()
            .pivot(index=["date", "dis"], columns="Dtype", values="number")
            .reset_index()
        )
        # display(dfCon)
        dfCon.columns = ["date", "dis", "隔离无症状", "隔离确诊病例"]
    else:
        return dfTotal

    dfout = (
        pd.merge(
            dfTotal,
            dfCon,
            left_on=["date", "dis"],
            right_on=["date", "dis"],
            how="outer",
        )
        .sort_values(by=["date", "dis"])
        .fillna(0)
    )

    return dfout


def detialForeMateV3(text, Patten_dic, date):
    tmp = []
    pationL = re.findall(pattern=Patten_dic["Patten_Pations"], string=text)
    for t in text.split("。"):
        if "均为本市闭环" in t:
            types = "闭环管控"
        elif "风险人群筛查" in t:
            types = "风险筛查"
        else:
            types = None
        if types:
            pationL = re.findall(pattern=Patten_dic["Patten_Pations"], string=t)
            pozL = re.findall(pattern=Patten_dic["Patten_poz"], string=t)
            pationL.extend(pozL)
            for d in pationL:
                dic = context(d)
                dic["conType"] = types
                dic["date"] = date
                tmp.append(dic)

    return dic2Table(tmp)


def detialForeMateV2(text, Patten_dic, date):
    tmp = []
    pationL = re.findall(pattern=Patten_dic["Patten_Pations"], string=text)
    for t in text.split("。"):
        
        if "均为本市闭环隔离管控人员" in t:
            types = "闭环管控"
        elif "风险人群筛查" in t:
            types = "风险筛查"
        else:
            types = None
        # print(t, types)

        if types:
            pationL = re.findall(pattern=Patten_dic["Patten_Pations2"], string=t)
            pozL = re.findall(pattern=Patten_dic["Patten_poz2"], string=t)
            pationL.extend(pozL)
            for d in pationL:
                dic = {}
                o = list(d)
                dic["Dtype"] = "确诊" if "病例" in o[0] else "无症状"
                # dic["gender"] = o[1]
                # dic["age"] = o[2]
                dic['number'] = 1
                dic["dis"] = o[-1]
                dic["conType"] = types
                dic["date"] = date
                tmp.append(dic)
                # if '浦东新区' not in d and '无症状' not in d:
                #     print(d, dic)
    # display(pd.DataFrame(tmp))
    return dic2Table(tmp)


Patten_dic = {
    "Patten_Date": r"(\d+)年(\d+)月(\d+)日",
    "Patten_daily": r"新增本土新?冠?肺?炎?确诊病例(\d+)例?和无症状感?染?者?(\d+)例?",
    'Patten_daily2': r"(\d+)例确诊病例和(\d+)例无症状感染者在隔离管控中",
    "Patten_Reload": r"其中(\d+)例*确诊病例为此前无症状感染者转归",
    "Patten_Out": r"治愈出院(\d+)例*",
    "Patten_Pations": r"(病例\d+—病例\d+|病例\d+|病例\d+、病例\d+)，居住于(.*?区)，",
    "Patten_poz": r"(无症状感染者\d+—无症状感染者\d+|无症状感染者\d+|无症状感染者\d+、无症状感染者\d+)，居住于(.*?区)，",
    "Patten_Pations2": r"(病例\d+)，(.)，(\d+岁)，居住于(.*?区)",
    "Patten_poz2": r"(无症状感染者\d+)，(.)，(\d+岁)，居住于(.*?区)",
}

tmpM = []
df = pd.DataFrame()
for path in fileDefinder("新增本土新冠肺炎确诊病例"):
    print("htmls/" + path)
    text = loadText("htmls/" + path).replace("\n", "").replace("\t", "").replace("\xa0",'')
    # print(text)
    date = "-".join(re.findall(pattern=Patten_dic["Patten_Date"], string=text)[0])
    date = datetime.datetime.strptime(date, "%Y-%m-%d")

    dailyPositive = DataReMainCountext(text, Patten_dic)
    dailyPositive["date"] = date
    tmpM.append(dailyPositive)

    # print(date, date >= datetime.datetime.strptime("2022-03-26", "%Y-%m-%d"))
    if date >= datetime.datetime.strptime("2022-03-26", "%Y-%m-%d"):
        tmp = detialForeMateV3(text, Patten_dic, date)
    else:
        tmp = detialForeMateV2(text, Patten_dic, date)
    df = pd.concat([df, tmp])

dfM = pd.DataFrame(tmpM)
dfM.columns = ["确诊病例", "无症状感染者", "隔离确诊病例", "隔离无症状", "转归", "治愈出院", "dis", "date"]

df = pd.concat([df, dfM]).sort_values(by=["date", "dis"]).fillna(0)
df.reset_index(inplace=True)
df.drop(columns="index", inplace=True)
df.info()
print(df.head())
df.to_csv("sumUPoutput.csv", index=False)
#%%
