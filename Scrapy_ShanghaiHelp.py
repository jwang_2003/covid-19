# %%
import requests
import json
import time
import random
import pandas as pd
import CSV2SQLSERVER as csv2sql


sqlSetting = {
    "server": "81.68.245.115",
    "user": "sa",
    "password": "SenseOfBA@021",
    "database": "Omicron_Shanghai",
}
keyMap = {
    "road": "Road",
    "dis": "District",
    "Dis": "District",
    "Date": "Dates",
    "date": "Dates",
    "status": "Status",
    "add": "Adderss",
    "street": "Street",
    "business_area":"BusinessArea",
    "type":"Types",
    "确诊":"ConfirmCase",
    "无症状":"Asymptomatic",
    "闭环无症状":"AsymptomaticInCycle",
    "闭环确诊":"ConfirmCaseInCycle",
    "风险筛查无症状":"AsymptomaticRisk",
    "风险筛查确诊":"ConfirmCaseRisk",
    "转归":'Recall'
}

#%%
class HelpRequest(object):
    def __init__(self, batchSize: int = 500, dateLimit=""):
        self.Header: str = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0"
        }
        self.API_Link: str = "https://api.helpothers.cn//help/list?"
        self.currentPage: int = 0

        self.dateLimit = dateLimit
        self.limit: int = batchSize
        self.start: int = 0
        self.totalRows: int = 0

    @staticmethod
    def dataReForm(inputDict: dict):
        outputList = []
        for i in inputDict:
            tag = i["tags"]
            i["tags2"] = tag
            if i["province"] == "直辖市" or i["province"] is None:
                i["province"] = i["city"]

            if "," in i["tags2"]:
                tags = i["tags2"].split(",")
                for t in tags:
                    d = i.copy()
                    d["tags2"] = t
                    outputList.append(d)
            else:
                outputList.append(i)
        return outputList

    def MainRequestLoop(self):
        """
        对Help页面中API信息进行获取
        """
        PageLimit: int = 99999
        mainData: list = []
        tmpDate = ""
        while self.currentPage < PageLimit and tmpDate < self.dateLimit:
            res = requests.get(
                self.API_Link,
                headers=self.Header,
                params={"limit": self.limit, "start": self.start},
            )
            dic: dict = json.loads(res.text)
            if res.status_code == 200:
                if dic.get("code") == 0 and dic.get("message") == "成功":
                    self.start += self.limit - int(self.limit * 0.2)
                    self.currentPage = dic["pageNum"]
                    PageLimit = dic["totalPage"]
                    totalRecord = dic["totalRecord"]
                    for d in dic["data"]:
                        if d["createdAt"] > tmpDate:
                            tmpDate = d["createdAt"]
                    mainData.extend(self.dataReForm(dic["data"]))
            time2Sleep: int = self.limit * 0.3 + random.randint(0, 30)
            print(
                " page {} / out of {} | Rows {} / {} --> Sleeping {} ".format(
                    self.currentPage,
                    PageLimit,
                    self.start + int(self.limit * 0.2),
                    totalRecord,
                    time2Sleep,
                    tmpDate,
                )
            )
            if (
                totalRecord > self.start + int(self.limit * 0.2)
                and tmpDate < self.dateLimit
            ):
                time.sleep(time2Sleep)
        return mainData


#%%


def dataReForm(dic: dict):
    l = []
    for i in dic:
        copyedData = i.copy()
        City = copyedData.get("city") if copyedData.get("city") else " "
        if "上海" in City:
            if copyedData["province"] == "直辖市" or copyedData["province"] is None:
                copyedData["province"] = copyedData["city"]

            tags = copyedData["tags"].split(",")

            dis = copyedData["county"]
            dis = "浦东新区" if "浦东" in dis else dis
            dis = "静安区" if "闸北" in dis else dis
            dis = dis.replace("区区", "区")
            if "区" not in dis:
                dis = dis + "区"
            copyedData["dis"] = dis

            for t in tags:
                lens = tags.index(t)
                copyedData["tags_{}".format(lens)] = t
            l.append(copyedData)
    return l


# .drop_duplicates().to_csv('datasource.csv',index=False)
#%%
# todo 修改匹配关系，能否通过循环列表方式进行匹配
# ! 注意可能会造成结果误差
def reGroupFunc(x):
    types = ""
    if "癌" in x or "化疗" in x or "肿瘤" in x or "白血病" in x or "血小板" in x:
        types = "癌症|肿瘤"
    elif "糖尿病" in x or "胰岛素" in x:
        types = "糖尿病"
    elif (
        "高血压" in x
        or "脑梗" in x
        or "心" in x
        or "血" in x
        or "中风" in x
        or "房颤" in x
        or "瓣膜" in x
    ):
        types = "心血管"
    elif (
        "吃" in x
        or "食物" in x
        or "食品" in x
        or "食材" in x
        or "蔬菜" in x
        or "厨房" in x
        or "面" in x
        or "粮" in x
        or "团购" in x
        or "奶" in x
        or "菜" in x
        or "肉" in x
        or "干粮" in x
        or "米" in x
        or "断粮" in x
        or "奶粉" in x
        or "干货" in x
        or "油" in x
        or "主食" in x
        or "调料" in x
        or "调味" in x
        or "冰箱" in x
        or "盐" in x
        or "饭" in x
        or "热食" in x
        or "豆" in x
        or "麦" in x
    ):
        types = "食品物资"
    elif (
        "宝宝" in x
        or "孕" in x
        or "产检" in x
        or "纸尿" in x
        or "帮宝适" in x
        or "尿不湿" in x
        or "胎" in x
        or "婴儿" in x
        or "产后" in x
        or "新生儿" in x
        or "产院" in x
    ):
        types = "母婴"
    elif "抑郁症" in x or "精神" in x or "抑郁" in x or "情绪" in x:
        types = "精神"
    elif "牙" in x or "龈" in x or "齿" in x or "口腔" in x:
        types = "口腔科"
    elif "安眠药" in x or "失眠" in x or "瘫痪" in x or "神经" in x or "癫痫" in x or "睡" in x:
        types = "神经"
    elif "肾" in x or "透析" in x or "血透" in x or "尿" in x:
        types = "肾课"
    elif "胃" in x or "肠" in x or "便" in x or "胆" in x or "食道" in x:
        types = "消化科"
    elif "眼" in x or "镜" in x or "失明" in x or "角膜" in x or "视" in x:
        types = "眼科"
    elif (
        "核酸" in x
        or "抗原" in x
        or "阳性" in x
        or "新冠" in x
        or "呕吐" in x
        or "烧" in x
        or "腹泻" in x
        or "检测" in x
    ):
        types = "新冠"
    elif "咳嗽" in x or "肺" in x:
        types = "呼吸道"
    elif (
        "医院" in x
        or "手术" in x
        or "骨" in x
        or "重症" in x
        or "重度" in x
        or "晚期" in x
        or "衰竭" in x
        or "治疗" in x
        or "医疗" in x
        or "综合" in x
        or "烫" in x
        or "发作" in x
        or "急诊" in x
        or "急救" in x
    ):
        types = "就医需要"
    elif (
        "症状" in x
        or "急性" in x
        or "病" in x
        or "患" in x
        or "失禁" in x
        or "呼吸" in x
        or "脱水" in x
        or "炎" in x
        or "脓" in x
        or "腰" in x
        or "疼" in x
        or "痛" in x
        or "感染" in x
        or "过敏" in x
        or "红肿" in x
        or "疗程" in x
        or "疗" in x
        or "溃疡" in x
        or "慢性" in x
        or "肿" in x
        or "抽搐" in x
    ):
        types = "其他症状"
    elif "卫生巾" in x or "姨妈" in x or "月经" in x:
        types = "妇女妇科"
    elif (
        "药" in x
        or "注射" in x
        or "处方" in x
        or "氧" in x
        or "盐酸" in x
        or "激素" in x
        or "尿道管" in x
        or "布洛芬" in x
        or "导尿" in x
        or "胶囊" in x
        or "片" in x
        or "复方" in x
        or "液" in x
        or "膏" in x
        or "水解" in x
        or "维生素" in x
        or "酸" in x
        or "阿" in x
        or "甲" in x
        or "氨" in x
        or "卡" in x
        or "氯" in x
    ):
        types = "治疗类物资"
    elif "猫" in x or "狗" in x or "仓鼠" in x or "流浪" in x or "宠物" in x:
        types = "宠物"
    elif "口罩" in x or "防疫" in x or "防护服" in x or "疫苗" in x or "防" in x or "卫生" in x:
        types = "防疫物资"
    elif (
        "网" in x
        or "物资" in x
        or "电脑" in x
        or "课" in x
        or "打印机" in x
        or "烟" in x
        or "笔" in x
        or "电" in x
        or "线" in x
        or "锅" in x
        or "纸" in x
        or "燃气" in x
        or "初中" in x
    ):
        types = "生活需要"
    elif "老" in x or "残疾" in x:
        types = "特需老人"
    elif "宾馆" in x or "酒店" in x or "火车站" in x or "房" in x or "住" in x:
        types = "住宿需要"
    elif (
        "物" in x
        or "配送" in x
        or "美团" in x
        or "京" in x
        or "东" in x
        or "求购" in x
        or "件" in x
    ):
        types = "物流"
    elif "湿疹" in x or "皮肤" in x:
        types = "皮肤"
    elif "谢谢" in x or "区" in x or "居" in x:
        types = "其他"
    else:
        types = "暂无分类"
    return types


#%%
dfP = pd.read_csv("DataSource/RequirmentData.csv")
dicData = HelpRequest(batchSize=100, dateLimit=dfP["createdAt"].max()).MainRequestLoop()
#%%
l = dataReForm(dicData)
df = pd.DataFrame(l)
df = pd.concat([dfP, df])
df.fillna("", inplace=True)
df["tags"].astype(str)
df.info()
#%%
df["province"].fillna(df["city"], inplace=True)
df["followStatus"].fillna("待跟进", inplace=True)
df["followStatus"] = df["followStatus"].apply(lambda x: "待跟进" if x =="" or x ==" " else x )
df["date"] = pd.to_datetime(df["createdAt"]).dt.date
df.drop_duplicates(subset=["id",'createdAt'], inplace=True)
df["retag"] = df["tags"].apply(reGroupFunc)
df = df[
    [
        "id",
        "helpLevel",
        "type",
        "tags",
        "province",
        "city",
        "county",
        "street",
        "followStatus",
        "followTime",
        "createdAt",
        "dis",
        "tags_0",
        "tags_1",
        "date",
        "retag",
    ]
]

print(df["dis"].unique().tolist())
#%%
# dfGB = df[['id','createdAt','followTime']].groupby('id').agg({'createdAt':'min','followTime':'max'}).reset_index()
# dfGB.columns = ['id','idFristCreateTime','idLastFollowTime']
# dfGB
# df = pd.merge(df,dfGB, on='id')
#%%
# df[df['retag'] == '暂无分类']['tags']
df.info()
df.to_csv("DataSource/RequirmentData.csv", index=False)
dfCsv = df.sort_values(["createdAt"]).fillna("null")
dfDic1 = dfCsv.to_dict("record")
csv2sql.SqlInput(
    dfDic1,
    inputTableName="Omicron_Shanghai.ODS.DailyRequirment",
    sqlSetting=sqlSetting,
    keyMap=keyMap,
    batchSize = 1000
)
# %%
