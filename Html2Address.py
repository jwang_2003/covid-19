# %%
import os
import re
import pandas as pd
import datetime
from lxml import etree


def fileDefinder(KeyWords="", path="htmls", ftype="html"):
    fList = []
    for p in os.listdir(path):
        if KeyWords in p and p.endswith(ftype):
            fList.append(path + "/" + p)
    return fList


# %%


def Html2AddrSource(filepath):
    with open(filepath, encoding="utf") as f:
        file = f.read()

    html = etree.HTML(file)
    sectionList = html.xpath(
        "//div[@id='js_content']//section[@data-tools='135编辑器']//p"
    )

    date = ""
    dis = ""
    dailyConferm = 0
    dailyunConferm = 0

    addList = []
    dailyList = []

    for s in sectionList:
        text = "".join(s.xpath(".//text()"))
        t_date = re.findall(r"(\d+)年(\d+)月(\d+)日", text)
        if t_date != [] and "新增" in text and ("居住" in text or '其涉及' in text):
            print(text)
            t_dis = re.findall(r"(.*区)|(.*)无新增|(.*)新增", text.split("，")[1])
            t_dailyConferm = re.findall(r"(\d+)(?=例?本?土?新?冠?肺?炎?本?土?确诊病例?)", text)
            t_dailyNoneConferm = re.findall(
                r"(\d+)(?=例?本?土?新?冠?肺?炎?无症状感染者本?土?例?)", text
            )
            date = datetime.date(
                int(t_date[0][0]), int(t_date[0][1]), int(t_date[0][2])
            )
            dis = (
                (
                    "".join(t_dis[0])
                    if "区" in "".join(t_dis[0])
                    else "".join(t_dis[0]) + "区"
                )
                if t_dis != []
                else None
            )
            dailyConferm = t_dailyConferm[0] if t_dailyConferm != [] else 0
            dailyunConferm = t_dailyNoneConferm[0] if t_dailyNoneConferm != [] else 0
            dailyList.append(
                {"Dis": dis, "Date": date, "确诊": dailyConferm, "无症状": dailyunConferm}
            )
            # print({"Dis": dis, "Date": date, "确诊": dailyConferm, "无症状": dailyunConferm})
        elif text != "" and t_date == []:
            # print(text)
            disTmp = (
                "".join(re.findall(r"^(.+?镇街道)|^(.+?镇)|^(.+?工业区)|^(.+?街道)", text)[0])
                if re.findall(r"^(.+?镇街道)|^(.+?镇)|^(.+?工业区)|^(.+?街道)", text) != []
                else None
            )
            roadList = re.findall(
                r"(?!.*镇街道)(?!.*镇)(?!.*工业区)(?!.*街道)(.+?)(?<=、|，|。)", text
            )
            if disTmp and len(roadList) > 1:
                # print(disTmp, roadList)
                for roadName in roadList:
                    # print(dis, disTmp + roadName)
                    addList.append(
                        {
                            "road": disTmp + roadName,
                            "status": "",
                            "date": date,
                            "dis": dis,
                        }
                    )
            else:
                addList.append({"road": text, "status": "", "date": date, "dis": dis})
        t_date = []

    dfAdd = pd.DataFrame(addList)
    dfAdd.drop(
        index=dfAdd[
            (dfAdd["date"] == "")
            | (dfAdd["road"] == "已对相关居住地落实终末消毒措施。")
            | (dfAdd["road"] == "")
        ].index,
        inplace=True,
    )
    dfDaily = pd.DataFrame(dailyList)
    dfAdd.to_excel(
        "SourceHTMLs/Data/{} 疫情Source.xlsx".format(datetime.datetime.strftime(date, "%Y-%m-%d")),
        index=False,
    )
    dfDaily.to_excel(
        "tmp/{} dailySum.xlsx".format(datetime.datetime.strftime(date, "%Y-%m-%d")),
        index=False,
    )
    return dfAdd, dfDaily


# %%
dfAddL, dfDailyL = [], []
paths = "dailyAdd"
filepaths = fileDefinder(KeyWords="本市各区确诊病例", path="SourceHTMLs/dailyAdd", ftype="htm")
for fpath in filepaths:
    dfAdd, dfDaily = Html2AddrSource(fpath)
    dfAddL.append(dfAdd)
    dfDailyL.append(dfDaily)

dfAdds = pd.concat(dfAddL).sort_values(["date", "dis"])

#%%
dfAdds.to_csv('DataSource/AddList.csv', index=False)
# dfAdd.to_excel(
#     "data/{} 疫情Source_Tmp.xlsx".format(datetime.datetime.strftime(date, "%Y-%m-%d")),
#     index=False,
# )
# %%
preDt = pd.read_csv('DataSource/DailySummary.csv',
# encoding='gbk'
)
#%%
preDt['Date'] = pd.to_datetime(preDt['Date']).dt.date
df = pd.concat(dfDailyL).sort_values(["Date", "Dis"])
df['Date'] = pd.to_datetime(df['Date']).dt.date

dfNextDay = df[df['Date'] == df['Date'].max()][['Date','Dis']]
dfNextDay['Date'] = dfNextDay['Date'] + pd.Timedelta(days=1)

df = pd.concat([preDt, df,dfNextDay]).fillna(0)
df[['确诊','无症状']] = df[['确诊','无症状']].astype(float)
df['Date'] = pd.to_datetime(df['Date']).dt.date
df.sort_values(['Date','Dis','无症状','确诊'])
df.drop_duplicates(['Date','Dis'] ,keep='last',inplace=True)
df.info()
df.to_csv("DataSource/Daily.csv", index=False)
print(df['Date'].max())
#%%

