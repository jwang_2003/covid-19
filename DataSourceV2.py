#%%
from datetime import timedelta, datetime
import pandas as pd

DayGap = 30
DayGap = datetime.now() + timedelta(days=-DayGap)
DayGap = DayGap.strftime('%Y-%m-%d')
#%%
# !Daily data
df_dailyDetial = pd.read_csv("DataSource/dailyDetial.csv")
df_dailyDetial.columns = ['Date', 'Dis', '转归', '闭环无症状', '闭环确诊', '风险筛查无症状', '风险筛查确诊']
df_dailyOverView = pd.read_csv("DataSource/Daily.csv")
df = pd.concat([df_dailyDetial,df_dailyOverView], axis=0).fillna(0)
dfDailyTotal = df.groupby(by=['Date','Dis']).sum().reset_index()
dfDailyTotal["Date"] = pd.to_datetime(dfDailyTotal["Date"]).dt.date.astype(str)
dfDailyTotal[['Dis','Date', '转归', '闭环无症状', '闭环确诊', '风险筛查无症状', '风险筛查确诊',
  '确诊', '无症状']].fillna(0).to_csv('DataSource/dailytotal.csv',index=False)
dfDailyTotal.drop(index= dfDailyTotal[dfDailyTotal['Date'] >= datetime.now().date().strftime('%Y-%m-%d')].index, inplace=True)
dfDailyTotal.fillna(0, inplace=True)
dfDailyTotal["确诊"] = dfDailyTotal[["确诊", "无症状", "转归", "闭环无症状", "闭环确诊", "风险筛查无症状", "风险筛查确诊"]].apply(
    lambda x: x["确诊"]
    if x["转归"] + x["闭环确诊"] + x["风险筛查确诊"] == 0
    else x["转归"] + x["闭环确诊"] + x["风险筛查确诊"],
    axis=1,
)
dfDailyTotal["无症状"] = dfDailyTotal[["确诊", "无症状", "转归", "闭环无症状", "闭环确诊", "风险筛查无症状", "风险筛查确诊"]].apply(
    lambda x: x["无症状"] if x["闭环无症状"] + x["风险筛查无症状"] == 0
    else x["闭环无症状"] + x["风险筛查无症状"],
    axis=1,
)
dfDm14 = (dfDailyTotal[dfDailyTotal['Date'] >= DayGap]
.groupby(by=['Date','Dis']).sum().reset_index())
dfDm14.rename(columns= dict(zip(dfDm14.columns,
['Dates', 'District', 'Recall', 'AsymptomaticInCycle', 'ConfirmCaseInCycle',
 'AsymptomaticRisk', 'ConfirmCaseRisk', 'ConfirmCase', 'Asymptomatic'])), inplace=True)
dfDm14 = dfDm14[['Dates', 'District', 'Recall', 'AsymptomaticInCycle', 'ConfirmCaseInCycle',
 'AsymptomaticRisk', 'ConfirmCaseRisk']]
dfDm14 = pd.melt(dfDm14,id_vars=['Dates','District'])
dfDm14.to_csv('DataSource/DataSource V2/lastNDayDailyInfo.csv', index=False)
#%%
# !build New Add List
dfAdd = pd.read_csv('DataSource/test.csv')
dfAdd = dfAdd[dfAdd['date'] >= DayGap][['dis','date','add','loc']]
dfAdd.sort_values('date', inplace=True)
#%%
dfAdd.rename(columns=dict(zip(dfAdd.columns, 
['District','Dates','Adderss','loc'])), inplace=True)
dfAddGb = dfAdd[['District','Dates','Adderss']].groupby(by=['District','Adderss']).agg({'Dates':['max','min','nunique','unique']}).reset_index()
dfCal = []
for c in dfAddGb.columns:
    cl = list(c)
    if c[1] == '':
        dfCal.append(cl[0])
    else:
        dfCal.append('_'.join(list(cl)))

dfAddGb.columns = dfCal
dfAdds = dfAdd.sort_values('Dates').drop_duplicates('Adderss',keep='last')
dfAddGb = pd.merge(dfAddGb,dfAdds,on=['District','Adderss'])
dfAddGb[['Longitude','Latitude']] = dfAddGb['loc'].str.split(',',expand=True)
dfAddGb.drop(columns='loc', inplace=True)
dfAddGb.to_csv('DataSource/DataSource V2/lastNdayAdderssList.csv',index=False)

#%%
dfFcst = pd.read_csv("DataSource/prod.csv")
dfFcst.to_csv('DataSource//DataSource V2/DailyForecast.csv')