#%%
import pandas as pd
import json


with open('shanghai.json',encoding='utf8') as f:
    maps = json.loads(f.read())

maps['districts'][0]['districts'][0]['districts']


#%%
l = []
for i in maps['districts'][0]['districts'][0]['districts']:
    # print(i['name'], i['adcode'], i['center'])
    disc = i['name']
    gps = i['center']
    Latitude = gps.split(',')[0]
    Longitude = gps.split(',')[1]
    dic = {'邮政编码':disc,'Area Code':disc,'adcode':i['adcode'], 'Latitude':Latitude, 'Longitude':Longitude}
    l.append(dic)
    for x in i['districts']:
        # print(' ->',x['name'],x['adcode'],x['center'])
        gps = i['center']
        Latitude = gps.split(',')[0]
        Longitude = gps.split(',')[1]
        dic = {'邮政编码':disc,'Area Code':x['name'],'adcode':x['adcode'], 'Latitude':Latitude, 'Longitude':Longitude}
        l.append(dic)
df = pd.DataFrame(l)
df['State/Province'] = '上海市'
df['Country (Name)'] = '中国'
# df.rename(columns={'国家':'Country (Name)'})
df
#%%

df.to_csv('ShanghaiLoac.csv')
#%%