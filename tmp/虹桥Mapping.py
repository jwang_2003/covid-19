# %%
from time import sleep
from tqdm import tqdm
import pandas as pd
import requests
import json

# %%
df = pd.read_excel('表2-明细表（虹桥镇）.xls')
df.columns
dfC = df.iloc[1:3].T[1].fillna('').astype(str) + df.iloc[1:3].fillna('').T[2].astype(str)
df.columns = dfC.tolist()
df.drop(index=[0,1,2], inplace=True)
df['区'] = '闵行区'
df['road'] = df['区'] + df['街道/镇'] + df['居住小区/自然村/单位/场所名称']
targetLocationName = df.to_dict('record')



# %%
l = []

for detials in tqdm(targetLocationName):
    tmp = detials.copy()
    roadName = detials["road"]
    # print(detials)
    roadName = roadName.replace("居住于", "")
    quest_url = "https://restapi.amap.com/v3/geocode/geo?"
    params = {
        "address": roadName,
        "city": '上海市',
        "output": "json",
        "key": "28376677293db5afe08b32bf1146314d",
    }
    res = requests.get(url=quest_url, params=params)
    text = json.loads(res.text)
    # print(text)
    if text.get("geocodes") is not None and res.status_code == 200 and text["info"] == "OK":
        if text["geocodes"] != []:
            t = text["geocodes"][0]
            tmp["loc"] = t["location"]
            tmp["add"] = t["formatted_address"]
            tmp['dis'] = t['district']
            tmp['adcode'] = t['adcode']
            tmp['street'] = t['street']
            tmp['business_area'] = t.get('business_area')
            l.append(tmp)
    else:
        print(text.get("geocodes"), res.status_code, 
        text.get("info"), '\n', text , '\n', params)
        l.append(detials)
    sleep(1 / 190)

# %%
df = pd.DataFrame(l)
df = df[['区', '街道/镇', '居住小区/自然村/单位/场所名称', '14天内无阳性感染者（√选）', 
'road', 'loc', 'add', 'dis']].replace({'√':"1"}).fillna('0')
df
# %%
tdf = pd.read_csv('test.csv').sort_values('date',ascending=False).drop_duplicates(subset=['road','dis'],keep='first')
tdf = tdf[['road', 'date','dis', 'add']]
tdf
#%%
pd.merge(df,tdf,left_on=['add'], right_on=['add'], how='left').info()



# .to_csv('lockdown.csv')