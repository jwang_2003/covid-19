#%%
import pandas as pd



df = pd.read_csv("test.csv")
df.date = pd.to_datetime(df.date).dt.date
df.drop_duplicates(subset=['road','add','date'],inplace=True)
df.info()
df.sort_values(by=['road','date'],ascending=True)
df.to_csv("test.csv",index=False)

#%%
df = pd.read_excel("病例汇总 3月18日前.xlsx")
tmp = df.other.str.split("，", expand=True)
tmp.rename(columns={0:'病例情况',1:'性别',2:'年龄',3:'地址',4:'其他'}, inplace=True)
tmp

df = df.merge(tmp,left_index=True,right_index=True,how='outer')
df['病例情况'] = df['病例情况'].apply(lambda x: '确诊' if '病例' in x else '无症状')
df['年龄'].replace({'岁':'','居住于':''},inplace=True,regex=True)
df['地址'].replace({'居住于':'','就读于':''},inplace=True,regex=True )
df
#%%
df['区'] = df['地址'].apply(lambda x : x.split('区')[0] + '区' if '区' in x else None)
df.to_csv('lists.csv',index=False)
#%%