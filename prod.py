# %%
from datetime import timedelta, datetime
import numpy as np
from scipy import optimize
import pandas as pd

df_dailyDetial = pd.read_csv("DataSource/dailyDetial.csv")
df_dailyDetial.columns = ['Date', 'Dis', '转归', '闭环无症状', '闭环确诊', '风险筛查无症状', '风险筛查确诊']
df_dailyOverView = pd.read_csv("DataSource/Daily.csv")
#%%
df = pd.concat([df_dailyDetial,df_dailyOverView], axis=0).fillna(0)
df = df.groupby(by=['Date','Dis']).sum().reset_index()
df["Date"] = pd.to_datetime(df["Date"]).dt.date.astype(str)
# dfNextDay = df[df['Date'] == df['Date'].max()][['Date','Dis']]
# dfNextDay['Date'] = dfNextDay['Date'] + pd.Timedelta(days=1)
# dfNextDay = pd.concat([df, dfNextDay]).fillna(0)
df[['Dis','Date', '转归', '闭环无症状', '闭环确诊', '风险筛查无症状', '风险筛查确诊',
  '确诊', '无症状']].fillna(0).to_csv('DataSource/dailytotal.csv',index=False)
df.sum()
# df[df['Date'] == '2022-04-23'].sum()
# df[df['Date'] >= datetime.now().date().strftime('%Y-%m-%d')]
#%%
df.drop(index= df[df['Date'] >= datetime.now().date().strftime('%Y-%m-%d')].index, inplace=True)
df.fillna(0, inplace=True)
df["_确诊"] = df[["确诊", "无症状", "转归", "闭环无症状", "闭环确诊", "风险筛查无症状", "风险筛查确诊"]].apply(
    lambda x: x["确诊"]
    if x["转归"] + x["闭环确诊"] + x["风险筛查确诊"] == 0
    else x["转归"] + x["闭环确诊"] + x["风险筛查确诊"],
    axis=1,
)
df["_无症状"] = df[["确诊", "无症状", "转归", "闭环无症状", "闭环确诊", "风险筛查无症状", "风险筛查确诊"]].apply(
    lambda x: x["无症状"] if x["闭环无症状"] + x["风险筛查无症状"] == 0
    else x["闭环无症状"] + x["风险筛查无症状"],
    axis=1,
)
df[df['Date'] == '2022-04-23'].sum()
#%%
df['Date'] = pd.to_datetime(df['Date']).dt.date
# df.drop(index=df[df["Date"] == df["Date"].max()].index, inplace=True)
# df.sort_values("Date")
# %%
df["total"] = df._确诊 + df._无症状
dfSum = df.groupby("Date").sum()
dfSum.reset_index(inplace=True)
dfSum.sort_values("Date", inplace=True)
dfSum
#%%
# dfSum.fillna(0, inplace=True)
dfSum["MA3"] = dfSum.total.rolling(
    2
    # ,center=True
).mean()
dfSum["MA3"].fillna(0, inplace=True)

dfSum
#%%
dfDic = dfSum.to_dict("records")

last, cma, cmaLast = 0, 0, 0
ids = 0
for d in dfDic:
    ids += 1
    last += d["total"]
    cma += d["MA3"]
    MA3 = d["MA3"] if d["MA3"] != 0 else cmaLast
    cmaLast = d["MA3"]
    d["runningSum"] = last
    # d["CMA3Sum"] = cma
    d["RowID"] = ids
    d["MA3"] = MA3

dfSum = pd.DataFrame(dfDic)
# dfSum["CMA3Sum"] = dfSum['runningSum'].rolling(3,center=True).mean().fillna(dfSum['runningSum'])
dfSum["Last"] = dfSum["runningSum"].shift(1).fillna(0)
dfSum = dfSum[
    [
        "RowID",
        "Date",
        "_确诊",
        "_无症状",
        "total",
        "runningSum",
        "Last",
        #  "MA3", "CMA3Sum"
    ]
]

_dfSum = dfSum.dropna(how="any")
currentLen = _dfSum.shape[0]
_dfSum.plot()
# dfSum = dfSum.iloc[2:]
# _dfSum = _dfSum[_dfSum['Date'] >= '2022-03-05']
# %%
refColumn = "runningSum"
movingDays = 20
start = 1

x0 = _dfSum["RowID"].astype(int).to_numpy()
y0 = _dfSum[refColumn].astype(int).to_numpy()
tmp = [0]
totalL = _dfSum["total"].tolist()
DateL = _dfSum["Date"].tolist()
RS = _dfSum["runningSum"].tolist()
# MA3s = _dfSum["MA3"].tolist()
# MA3R = _dfSum["CMA3Sum"].tolist()

# def func1(x, a, b, c):
#     return a * np.exp(b * x) + c
# a1, b1, c1 = optimize.curve_fit(func1, x0, y0)[0]
# print(a1, b1, c1)


def func1(x, a, b, c, d):
    return a * x ** 3 + b * x ** 2 + c * x + d


pearm = optimize.curve_fit(func1, x0, y0)[0]
# print(pearm)
# x1 = [i + start for i in range(currentLen + int(movingDays * (1/20)))]
# x1 = [i + start for i in range(currentLen + int(1))]
# x1 = [i + start for i in range(currentLen)]
# yp0 = func1(np.array(x1), pearm[0], pearm[1], pearm[2], pearm[3])

# dict(zip(y0 ,yp0))
x1 = [i + start for i in range(currentLen + 3)]
yp0 = func1(np.array(x1), pearm[0], pearm[1], pearm[2], pearm[3])
# print(yp0)
runningSums = _dfSum["runningSum"]


def func2(x, b, c, d):
    return (1 + c) / (1 + np.exp(d + b * -x))


b1, c1, d1 = optimize.curve_fit(func2, x0, y0)[0]
print(b1, c1, d1)
x1 = [i + start for i in range(currentLen + movingDays)]


yp = func2(np.array(x1), b1, c1, d1)
yp
#%%
# def func3(x, a, b):
#     return a * np.arctan(x) + b

# a2, b2 = optimize.curve_fit(func3, x0, runningSums)[0]
# a1, b1, x0
# x1 = [i + start for i in range(currentLen + movingDays)]
# yp2 = func3(np.array(x1), a1, b1)
# yp2


l = []
for i in x1:
    ind = i - start
    rowID = x1[ind]
    Prodict = yp[ind]
    ProdE = yp0[ind] if len(yp0) > ind else None
    actual = y0[ind] if len(y0) > ind else None
    total = totalL[ind] if len(totalL) > ind else None
    running = RS[ind] if len(RS) > ind else None
    # _MA3s = MA3s[ind] if len(MA3s) > ind else None
    # _MA3R = MA3R[ind] if len(MA3R) > ind else None
    Date = (
        DateL[ind] if len(DateL) > ind else max(DateL) + timedelta(days=i - len(DateL))
    )
    dic = {
        "refProdict": ProdE,
        "rowID": rowID,
        "Prodict": Prodict,
        "actual": actual,
        "runningSum": running,
        "total": total,
        "Date": Date,
        # "MA3": _MA3s,
        # "MA3Sum": _MA3R,
    }
    l.append(dic)
df = pd.DataFrame(l)

df["last"] = df.Prodict.shift(1)
df["last"].fillna(0, inplace=True)
df["prod2"] = df["Prodict"] - df["last"]
df[
    [
        "total",
        "prod2",
        #  "MA3", "MA3Sum"
    ]
].plot(figsize=(15, 5))
df[
    [
        "runningSum",
        "Prodict"
        #  "MA3", "MA3Sum"
    ]
].plot(figsize=(15, 5))
# df['Value_a'] = a1
df["Value_b"] = b1
df["Value_c"] = c1
df["Value_d"] = d1
df.drop(columns="last")
df.iloc[20:60, :]
#%%
df["error_total"] = df["Prodict"] - df["runningSum"]
df["error_daliy"] = df["prod2"] - df["total"]


_df = df.dropna(how="any")
MSE = 1 / _df.rowID.max() * (np.sum(np.square(_df["runningSum"] - _df.Prodict)))
MSE * (1 / 2)

df["R2_total"] = 1 - (np.sum(np.square(_df["runningSum"] - _df.Prodict))) / np.sum(
    np.square(_df["runningSum"].mean() - _df["runningSum"])
)
df["R2_daliy"] = 1 - (np.sum(np.square(_df.total - _df.prod2))) / np.sum(
    np.square(_df.total.mean() - _df.total)
)
1 - (np.sum(np.square(_df["runningSum"] - _df.Prodict))) / np.sum(
    np.square(_df["runningSum"].mean() - _df["runningSum"])
), 1 - (np.sum(np.square(_df.total - _df.prod2))) / np.sum(
    np.square(_df.total.mean() - _df.total)
)
#%%
df['pub_date'] = datetime.now() + timedelta(days=-1)
df['pub_date'] = df['pub_date'].dt.date
df['pub_date'] = pd.to_datetime(df['pub_date']).dt.date
df['Date'] = pd.to_datetime(df['Date']).dt.date
df

#%%
# df = pd.concat([preDf, df])
df.fillna("",inplace=True)
df.drop_duplicates(subset=['Date','pub_date'], inplace=True)
df.info()

df.to_csv("DataSource/prod.csv",index=False)
df.to_csv("DataSource/DataSource V2/prod.csv",index=False)
print("done")
# %%
